import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John","123456536","Manila");
        Contact contact2 = new Contact("Jane","342545636","Ontario");
        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        if(phonebook.getContacts().isEmpty()){
            System.out.println("Phonebook is empty.");
        }else{
            for(Contact contact: phonebook.getContacts()){
                printInfo(contact);
            }
        }
    }

    public static void printInfo(Contact contact) {
        System.out.println("Name: " + contact.getName());
        System.out.println("Contact Number: " + contact.getContactNumber());
        if (contact.getAddress() != null) {
            System.out.println("Address: " + contact.getAddress());
        } else {
            System.out.println("Address: Not provided.");
        }
        System.out.println("--------------------------");
    }
}